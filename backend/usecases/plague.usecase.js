const plagueRepository = require('../repositories/db.plague');
const response = require('../model/response.model');
const util = require('../util/util');

var processDataPlagues = function (data, objResponse) {
    let plague = {};
    for (let i = 0; i < data.length; i++) {
        plague.plague_id = data[i].plague_id;
        plague.name_plague = data[i].name_plague;
        plague.name_clarifai = data[i].name_clarifai;
        plague.description_plague = data[i].description_plague;
        plague.url_image_plague = data[i].url_image;
        plague.control = [];
        objResponse.push(plague);
        plague = {};
    }
};

exports.getPlagues = function (plagueId) {
    return new Promise((resolve, reject) => {
        plagueRepository.getPlagues(plagueId).then((results) => {
            var dataPlague = [];
            processDataPlagues(util.cleanDuplicate(results, 'plague_id'), dataPlague);
            for (let i = 0; i < dataPlague.length; i++) {
                for (let y = 0; y < results.length; y++) {
                    if (results[y].plague_id === dataPlague[i].plague_id) {
                        results[y].name_plague = undefined;
                        results[y].name_clarifai = undefined;
                        results[y].description_plague = undefined;
                        results[y].plague_id = undefined;
                        results[y].url_image = undefined;
                        dataPlague[i].control.push(results[y]);
                    }
                }
            }
            if (dataPlague.length > 0) {
                resolve(response.getResponse("success", "success",
                    (plagueId !== undefined) ? dataPlague[0] : dataPlague));
            } else {
                reject(response.getResponse("error", "NOT_FUND", {}));
            }
        }).catch((error) => {
            reject(response.getResponse("error", error, {}));
        })
    });
};

var processData = function (data, objResponse) {
    let query = {};
    for (let i = 0; i < data.length; i++) {
        query.query_id = data[i].query_id;
        query.url_image = data[i].url_image_query;
        query.date = data[i].date;
        query.prediction = [];
        objResponse.push(query);
        query = {};
    }
};

exports.getHistoryRecognitions = function (obj) {
    return new Promise((resolve, reject) => {
        plagueRepository.getHistoryRecognitions(obj).then((results) => {
            var queries = [];
            processData(util.cleanDuplicate(results, 'query_id'), queries);
            for (let i = 0; i < queries.length; i++) {
                for (let y = 0; y < results.length; y++) {
                    if (results[y].query_id === queries[i].query_id) {
                        results[y].url_image_query = undefined;
                        results[y].query_id = undefined;
                        results[y].date = undefined;
                        queries[i].prediction.push(results[y]);
                    }
                }
                queries[i].prediction.sort((a, b) => Number(a.value) - Number(b.value)).reverse();
            }
            //queries.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
            queries.sort((a, b) => Number(a.query_id) - Number(b.query_id)).reverse();
            resolve(response.getResponse("success", "success", queries));
        }).catch((error) => {
            reject(response.getResponse("error", error, {}));
        })
    });
};