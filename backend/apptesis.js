// Server HTTP
var fs = require('fs');
var path = require('path');
var express = require('express');
var app = express();
var http = require('http');
var ExpressBrute = require('express-brute');
var morgan = require('morgan');
var bruteforce = new ExpressBrute(new ExpressBrute.MemoryStore(), {
    "freeRetries": 5500,
    "lifetime": 10
});
var cors = require('cors');
var bodyParser = require('body-parser');
var multer = require('multer');

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
    flags: 'a'
});
app.use(morgan('combined', {
    stream: accessLogStream
}));
app.use(require('helmet')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.options('*', cors());

//import routes
var userRoutes = require('./routes/user/user');
var authRoutes = require('./routes/auth/auth');

//import objects
var userObject = require('./model/user.model');

//import usecase
var userUseCases = require('./usecases/user.usecases');
var recognitionUseCases = require('./usecases/recognition.usecases');
const plaguetUseCases = require('./usecases/plague.usecase');

//import response
var response = require('./model/response.model');


/**
 * 200 OK - Respuesta a un exitoso GET, PUT, PATCH o DELETE. Puede ser usado también para un POST que no resulta en una creación.
 * 201 Created – [Creada] Respuesta a un POST que resulta en una creación. Debería ser combinado con un encabezado Location, apuntando a la ubicación del nuevo recurso.
 * 204 No Content – [Sin Contenido] Respuesta a una petición exitosa que no devuelve un body (como una petición DELETE)
 * 400 Bad Request – [Petición Errónea] La petición está malformada, como por ejemplo, si el contenido no fue bien parseado.
 * 401 Unauthorized – [Desautorizada] Cuando los detalles de autenticación son inválidos o no son otorgados. También útil para disparar un popup de autorización si la API es usada desde un navegador.
 * 403 Forbidden – [Prohibida] Cuando la autenticación es exitosa pero el usuario no tiene permiso al recurso en cuestión.
 * 404 Not Found – [No encontrada] Cuando un recurso no existente es solicitado.
 * 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación
 * 429 Too Many Requests – [Demasiadas peticiones] Cuando una petición es rechazada debido a la tasa límite .
 */
const HTTP_STATUS = {
    OK: 200,
    CREATED: 201,
    NO_CONTENT: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    UNPROCESSABLE: 200,
    TOO_MANY_REQUEST: 429
};

//helpers
var util = require('./util/util');
var fs = require('fs');

app.use('/api', bruteforce.prevent, userRoutes);
app.use('/auth', bruteforce.prevent, authRoutes);

app.get('/ping', bruteforce.prevent, function (req, res) {
    res.send({
        'status': true
    });
});

app.post('/users', bruteforce.prevent, function (req, res) {
    util.consoleLog("<- users");
    if (req.body.username !== undefined && req.body.username !== null && req.body.username.length >= 1 &&
        req.body.password !== undefined && req.body.password !== null && req.body.password.length >= 1 &&
        req.body.name !== undefined && req.body.name !== null && req.body.name.length >= 1 &&
        req.body.email !== undefined && req.body.email !== null && req.body.email.length >= 1) {
        var obj = userObject.getUserObject(req.body.username, req.body.password, req.body.name, req.body.email);
        userUseCases.createUser(obj).then(function (result) {
            util.consoleLog("-> users");
            res.status(HTTP_STATUS.OK).send(result);
        }).catch(function (error) {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        });
    } else {
        res.status(HTTP_STATUS.BAD_REQUEST).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});

function authorize(req, res, next) {
    util.consoleLog(req.query);
    if (req.query.user === 'mayer') {
        next();
    } else {
        res.status(403).send('Forbidden');
    }
};

app.get('/admin', bruteforce.prevent, authorize, function (req, res) {
    res.send("autorizado");
});

app.post('/login', bruteforce.prevent, function (req, res) {
    util.consoleLog("<- login ");
    if (req.body.username !== undefined && req.body.username !== null && req.body.username.length >= 1 &&
        req.body.password !== undefined && req.body.password !== null && req.body.password.length >= 1) {
        userUseCases.loginUser(req.body.username, req.body.password).then(function (result) {
            util.consoleLog("-> login");
            res.status(HTTP_STATUS.OK).send(result);
        }).catch(function (error) {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        });
    } else {
        res.status(HTTP_STATUS.UNPROCESSABLE).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});

/**
 * realiza la prediccion de la plaga, dado la url de la imagen
 */
app.post('/recognitions', bruteforce.prevent, (req, res) => {
    if (req.body.user !== undefined && req.body.user !== null &&
        req.body.urlImage !== undefined && req.body.urlImage !== null) {
        userUseCases.infoUser(req.body.user).then((results) => {
            let obj = {
                user: req.body.user,
                urlImage: req.body.urlImage,
                idUser: results.data.idUser
            }
            recognitionUseCases.recognize(obj).then((results) => {
                res.status(HTTP_STATUS.OK).send(results);
            }).catch((error) => {
                res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
            })
        }).catch((error) => {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        })
    } else {
        res.status(HTTP_STATUS.UNPROCESSABLE).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});

/**
 * listar el historico de predicciones con el top 5 de coincidencias mayores.
 */
app.post('/:user/recognitions', bruteforce.prevent, (req, res) => {
    if (req.body.user !== undefined && req.body.user !== null) {
        userUseCases.infoUser(req.body.user).then((results) => {
            let obj = {
                user: req.body.user,
                idUser: results.data.idUser
            };
            plaguetUseCases.getHistoryRecognitions(obj).then((results) => {
                res.status(HTTP_STATUS.OK).send(results);
            }).catch((error) => {
                res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
            })
        }).catch((error) => {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        })
    } else {
        res.status(HTTP_STATUS.UNPROCESSABLE).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});

/**
 * return detalles de la plaga
 */
app.post('/plagues/:id', bruteforce.prevent, (req, res) => {
    if (req.body.user !== undefined && req.body.user !== null &&
        req.body.plagueId !== undefined && req.body.plagueId !== null) {
        let obj = {
            user: req.body.user,
            plagueId: req.body.plagueId
        }
        plaguetUseCases.getPlagues(obj.plagueId).then((results) => {
            res.status(HTTP_STATUS.OK).send(results);
        }).catch((error) => {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        })
    } else {
        res.status(HTTP_STATUS.UNPROCESSABLE).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});

/**
 * listar todas las plagas del pepino dulce y demás
 */
app.post('/plagues', bruteforce.prevent, (req, res) => {
    if (req.body.user !== undefined && req.body.user !== null) {
        let obj = {
            user: req.body.user
        }
        plaguetUseCases.getPlagues().then((results) => {
            res.status(HTTP_STATUS.OK).send(results);
        }).catch((error) => {
            res.status(HTTP_STATUS.UNPROCESSABLE).send(error);
        })
    } else {
        res.status(HTTP_STATUS.UNPROCESSABLE).send(response.getResponse("error", "INVALID_PARAMETER_SIZE", {}));
    }
});


const PORT = process.env.PORT || 10000

var server = http.createServer(app);
server.listen(PORT, function () {
    util.consoleLog('Backend app listening on port ' + PORT + '!');
});
// for testing
module.exports = app;