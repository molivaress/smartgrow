var dbPool = require('./db_pool.mysql');
var util = require('../util/util');

exports.getPlagues = function (plagueId) {
    return new Promise(function (resolve, reject) {
        let filter = " ";
        let params = [];
        if (plagueId !== undefined) {
            filter = " where p.id = ?;";
            params.push(plagueId);
        }
        let sql = "select p.id as plague_id, p.name as name_plague, " +
            " p.name_clarifai as name_clarifai, p.url_image, p.description as description_plague, " +
            " c.type as type_control, c.description as description_control, " +
            " c.create_date " +
            " from plague as p" +
            "    join control as c on c.plague_id = p.id " + filter;
        //resolve(data);
        //return;
        dbPool.connection.query(sql, params).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) ? resolve(results): resolve([]);
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.plague.js in getPlagues: ", err);
            reject("DB_ERROR");
        });
    });
};

exports.getHistoryRecognitions = function (obj) {
    return new Promise(function (resolve, reject) {
        let sql = "select q.url_image as url_image_query, r.query_id, q.create_date as date, " +
            " p.name as name, r.concept as name_clarifai, p.url_image as url_image_plague,  r.value, " +
            " r.plague_id from query as q " +
            " join result as r on q.id = r.query_id" +
            " join plague as p on p.id = r.plague_id " +
            " where user_id = ? " +
            " order by q.id desc;";
        dbPool.connection.query(sql, [obj.idUser]).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) ? resolve(results): resolve([]);
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.plague.js in getHistoryRecognitions: ", err);
            reject("DB_ERROR");
        });
    });
};

var data = [{
    name_plague: "araña",
    description_plague: "description",
    control: [{
        type_control: 1,
        description_control: "control biologico"
    }, {
        type_control: 0,
        description_control: "control quimico"
    }]
}, {
    name_plague: "araña",
    description_plague: "description",
    control: [{
        type_control: 1,
        description_control: "control biologico"
    }, {
        type_control: 0,
        description_control: "control quimico"
    }]
}, {
    name_plague: "araña",
    description_plague: "description",
    control: [{
        type_control: 1,
        description_control: "control biologico"
    }, {
        type_control: 0,
        description_control: "control quimico"
    }]
}];