package com.olivares.smartgrow.views.login;

import com.olivares.entities.User;
import com.olivares.usecase.usecase.history.GetHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.plagues.GetPlaguesUseCase;
import com.olivares.usecase.usecase.user.GetUserAccountDataUseCase;
import com.olivares.usecase.usecase.user.LoginUserAccountUseCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginPresenterUnitTest {
    @Mock
    private LoginPresenter.LoginView loginView;
    @Mock
    private LoginPresenter.UserLoggedSessionView userLoggedSessionView;
    @Mock
    private LoginUserAccountUseCase loginUserAccountUseCase;
    @Mock
    private GetUserAccountDataUseCase getUserAccountDataUseCase;
    @Mock
    private GetHistoryRecognitionUseCase getHistoryRecognitionUseCase;
    @Mock
    private GetPlaguesUseCase getPlaguesUseCase;
    private LoginPresenter loginPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setNewThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        loginPresenter = new LoginPresenter(this.loginUserAccountUseCase, getUserAccountDataUseCase,
                getHistoryRecognitionUseCase, getPlaguesUseCase);
        loginPresenter.setLoginView(loginView);
        loginPresenter.setUserLoggedSessionView(userLoggedSessionView);
    }

    @Test
    public void testDestroy() {
        this.loginPresenter.destroy();
        assertNull(this.loginPresenter.getLoginView());
    }

    @Test
    public void testLoginUserSuccess() {
        try {
            User user = createUser();
            when(this.loginUserAccountUseCase.loginUser(user)).thenReturn(user);
            this.loginPresenter.loginUser(user);
            verify(this.loginView).showLoading();
        } catch (Exception e) {
            verify(this.loginView).hideLoading();
            verify(this.loginView).showError(e.getMessage());
        }
    }

    @Test
    public void testLoginUserFail() {
        try {
            User user = createUser();
            when(this.loginUserAccountUseCase.loginUser(user)).thenThrow(new Exception("Hello"));
            this.loginPresenter.loginUser(user);
            verify(this.loginView).showError("Hello");
        } catch (Exception e) {
            verify(this.loginView).hideLoading();
            verify(this.loginView).showError(e.getMessage());
        }
    }

    @Test
    public void testGoToHome() {
        try {
            this.loginPresenter.goToHome();
            verify(this.userLoggedSessionView).showHome();
        } catch (Exception e) {
            verify(this.userLoggedSessionView).hideLoading();
            verify(this.userLoggedSessionView).showError(e.getMessage());
        }
    }

    private User createUser() {
        return new User(1, "Test", "admin", "123");
    }

}
