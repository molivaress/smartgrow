package com.olivares.smartgrow.views.onboarding;

import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.entities.OnBoardItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class OnBoardingPresenter {

    OnBoardingView onBoardingView;

    @Inject
    public OnBoardingPresenter() {
    }

    public void setOnBoardingView(OnBoardingView onBoardingView) {
        this.onBoardingView = onBoardingView;
    }

    public void loadOnBoardingList() {
        // call to UseCase
        List<OnBoardItem> lista = new ArrayList<>();
        lista.add(new OnBoardItem(
                "Identificar",
                "Identifica la plaga que afecta a tus plantas de pepino dulce.",
                R.drawable.identify,
                "successfully_analize_onboard"));
        lista.add(new OnBoardItem(
                "Aprender",
                "Aprende sobre las plagas que afectan a tus plantas de pepino dulce.",
                R.drawable.learn,
                "procesando_server_analized"));
        lista.add(new OnBoardItem(
                "Control",
                "Evalúe las estrategias para controlar las plagas que afectan a tus plantas de pepino dulce.",
                R.drawable.control,
                "mobile_navigation"));
        this.onBoardingView.showOnBoardingList(lista);
    }

    public void showAnimation() {
        this.onBoardingView.showAnimation();
    }

    public void hideAnimation() {
        this.onBoardingView.hideAnimation();
    }

    public void goToLogin() {
        onBoardingView.goToLogin();
    }

    public interface OnBoardingView {
        void showOnBoardingList(List<OnBoardItem> onBoardItemList);

        void showAnimation();

        void hideAnimation();

        void goToLogin();

        void goToCreateAccount();
    }
}
