package com.olivares.smartgrow.views.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerHistoryComponent;
import com.olivares.smartgrow.di.components.DaggerLoginComponent;
import com.olivares.smartgrow.di.components.DaggerProfileComponent;
import com.olivares.smartgrow.di.components.HistoryComponent;
import com.olivares.smartgrow.di.components.ProfileComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.smartgrow.views.history.HistoryAdapter;
import com.olivares.smartgrow.views.history.HistoryPresenter;
import com.olivares.smartgrow.views.home.HomeActivity;
import com.olivares.smartgrow.views.plagues.PlaguesFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class ProfileActivity extends BaseActivity implements ProfilePresenter.ProfileView {
    private static final String TAG = ProfileActivity.class.getName();
    @BindView(R.id.imageBackgroundProfile)
    ImageView mImageViewBackground;
    @BindView(R.id.textNameUserProfile)
    EditText mEditTextNameUser;
    @BindView(R.id.textDescriptionProfile)
    EditText mEditTextDescriptionUser;
    @BindView(R.id.progressBar_profile_activity)
    ProgressBar mProgressBar;
    private static final int PROFILE_TYPE = 1;


    @Inject
    Picasso picasso;
    @Inject
    ProfilePresenter profilePresenter;
    ProfileComponent profileComponent;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        user = new User();
        setUpView();
        setUpDataProfile();
    }

    public static Intent getCallIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.profileComponent.inject(this);
        this.profilePresenter.setProfileView(this);
    }


    private void setUpDataProfile() {
        this.profilePresenter.getUser();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.profileComponent = DaggerProfileComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    @Override
    public void showProfile(User userAccount) {
        this.user = userAccount;
        mEditTextNameUser.setText(this.user.getName());
        mEditTextDescriptionUser.setText(this.user.getDescription());
        loadImagePicasso(mImageViewBackground, this.user.getUrlImage());
    }

    @Override
    public void getProfile(User user) {
        this.profilePresenter.loadUserProfile(user.getUsername());
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.ai)
                .error(R.drawable.ai)
                .into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_profile:
                updateProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateProfile() {
        Log.d(TAG, "showUpdateProfile");
        if (validTextView()) {
            this.user.setDescription(mEditTextDescriptionUser.getText().toString());
            this.user.setName(mEditTextNameUser.getText().toString());
            this.profilePresenter.updateProfile(this.user);
            NavUtils.navigateUpFromSameTask(this);
            //startActivity(HomeActivity.getCallIntent(getApplicationContext()));
        } else {
            Toast.makeText(getApplicationContext(), "Debe ingresar datos", Toast.LENGTH_LONG).show();
        }
    }

    public Boolean validTextView() {
        return !mEditTextNameUser.getText().toString().isEmpty();
    }

    @Override
    public void showLoading() {
        this.mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(String messageError) {
        this.mProgressBar.setVisibility(View.INVISIBLE);
        this.showToastMessage(messageError);
    }
}
