package com.olivares.smartgrow.views;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

public class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
