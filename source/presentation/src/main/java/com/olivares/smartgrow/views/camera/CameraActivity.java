package com.olivares.smartgrow.views.camera;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.olivares.smartgrow.R;
import com.olivares.smartgrow.views.camera.alert.AlertAdapter;

public class CameraActivity extends AppCompatActivity implements IMainActivity {
    public static final String TAG = CameraActivity.class.getName();
    private static final int REQUEST_CODE = 1234;
    public static String CAMERA_POSITION_FRONT;
    public static String CAMERA_POSITION_BACK;
    public static String MAX_ASPECT_RATIO;
    //widgets
    //vars
    private boolean mPermissions;
    public String mCameraOrientation = "none"; // Front-facing or back-facing


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        init();
    }

    public static Intent getCallIntent(Context context) {
        return new Intent(context, CameraActivity.class);
    }

    private void startCamera2() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.camera_container, CameraFragment.newInstance());
        transaction.commit();
    }

    private void init() {
        if (mPermissions) {
            if (checkCameraHardware(this)) {
                // Open the Camera
                startCamera2();
            } else {
                showSnackBar("You need a camera to use this application", Snackbar.LENGTH_INDEFINITE);
            }
        } else {
            verifyPermissions();
        }
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: asking user for permissions.");
        String[] permissions = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED) {
            mPermissions = true;
            init();
        } else {
            ActivityCompat.requestPermissions(CameraActivity.this, permissions, REQUEST_CODE);
        }
    }

    private void showSnackBar(final String text, final int length) {
        View view = this.findViewById(android.R.id.content).getRootView();
        Snackbar.make(view, text, length).show();
    }


    @Override
    public void setCameraBackFacing() {
        Log.d(TAG, "setCameraBackFacing: setting camera to back facing.");
        mCameraOrientation = CAMERA_POSITION_BACK;
    }

    @Override
    public boolean isCameraBackFacing() {
        if (mCameraOrientation.equals(CAMERA_POSITION_BACK)) {
            return true;
        }
        return false;
    }

    @Override
    public void setBackCameraId(String cameraId) {
        CAMERA_POSITION_BACK = cameraId;
    }

    @Override
    public String getBackCameraId() {
        return CAMERA_POSITION_BACK;
    }

    @Override
    public void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    public void showStatusBar() {
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    AlertAdapter alertAdapter;

    @Override
    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogTheme);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.custom_alert_dialog, null);
        builder.setPositiveButton("OK", null);
        builder.setView(dialogLayout);
        // show lista de imagenes
        recyclerView = dialogLayout.findViewById(R.id.recyclerView_alert);
        recyclerView.setHasFixedSize(true);
        alertAdapter = new AlertAdapter(getApplicationContext());
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(alertAdapter);
        builder.show();
    }
}
