package com.olivares.smartgrow.views.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.olivares.smartgrow.R;

public class ViewDialog {
    private Dialog dialog = null;
    private IListenerViewDialog iListenerViewDialog;

    public ViewDialog(Activity activity) {
        dialog = new Dialog(activity);
    }

    public void setiListenerViewDialog(IListenerViewDialog iListenerViewDialog) {
        this.iListenerViewDialog = iListenerViewDialog;
    }

    public void showDialog(String msg) {
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iListenerViewDialog.cancelProcess();
            }
        });
        dialog.show();
    }

    public void closeDialog() {
        dialog.dismiss();
    }

    public interface IListenerViewDialog {
        void cancelProcess();
    }
}
