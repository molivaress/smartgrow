package com.olivares.smartgrow.views;

public interface LoadView {
    void showLoading();

    void hideLoading();

    void showError(String messageError);
}
