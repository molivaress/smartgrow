package com.olivares.smartgrow.views;

public interface Presenter {
    void destroy();
}
