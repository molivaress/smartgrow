package com.olivares.smartgrow.di.modules;

import com.olivares.local.db.DbPlagueRepository;
import com.olivares.local.db.DbQueryRepository;
import com.olivares.local.db.DbUserRepository;
import com.olivares.local.prefs.PrefUserRepository;
import com.olivares.remote.cloud.history.CloudHistoryRecognitionRepository;
import com.olivares.remote.cloud.plagues.CloudPlaguesRepository;
import com.olivares.remote.cloud.login.CloudUserRepository;
import com.olivares.remote.cloud.photography.CloudPhotographyRepository;
import com.olivares.remote.cloud.recognize.CloudPlaguesRecognitionRepository;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryRemote;
import com.olivares.usecase.repository.photography.PhotographyRepositoryRemote;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryRemote;
import com.olivares.usecase.repository.user.UserRepositoryLocal;
import com.olivares.usecase.repository.user.UserRepositoryPref;
import com.olivares.usecase.repository.user.UserRepositoryRemote;
import com.olivares.usecase.repository.recognize.PlaguesRecognitionRepositoryRemote;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    UserRepositoryRemote loginRepositoryRemote(CloudUserRepository cloudLoginRepository) {
        return cloudLoginRepository;
    }

    @Provides
    @Singleton
    UserRepositoryLocal userRepositoryLocal(DbUserRepository dbUserRepository) {
        return dbUserRepository;
    }

    @Provides
    @Singleton
    UserRepositoryPref userRepositoryPref(PrefUserRepository prefUserRepository) {
        return prefUserRepository;
    }

    @Provides
    @Singleton
    PhotographyRepositoryRemote imageRepositoryRemote(CloudPhotographyRepository cloudImageRepository) {
        return cloudImageRepository;
    }

    @Provides
    @Singleton
    PlaguesRecognitionRepositoryRemote recognitionRepositoryRemote(CloudPlaguesRecognitionRepository cloudRecognitionRepository) {
        return cloudRecognitionRepository;
    }

    @Provides
    @Singleton
    PlaguesRepositoryRemote libraryRepositoryRemote(CloudPlaguesRepository cloudLibraryRepository) {
        return cloudLibraryRepository;
    }

    @Provides
    @Singleton
    PlaguesRepositoryLocal dbPlagueRepository(DbPlagueRepository dbPlagueRepository) {
        return dbPlagueRepository;
    }

    @Provides
    @Singleton
    HistoryRecognitionRepositoryRemote historyRepositoryRemote(CloudHistoryRecognitionRepository cloudHistoryRepository) {
        return cloudHistoryRepository;
    }

    @Provides
    @Singleton
    HistoryRecognitionRepositoryLocal historyRepositoryRemoteLocal(DbQueryRepository dbQueryRepository) {
        return dbQueryRepository;
    }
}
