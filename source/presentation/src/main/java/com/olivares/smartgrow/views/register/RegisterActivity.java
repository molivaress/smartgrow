package com.olivares.smartgrow.views.register;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.olivares.entities.User;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerRegisterComponent;
import com.olivares.smartgrow.di.components.RegisterComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.smartgrow.views.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterPresenter.RegisterView {
    private static final String TAG = RegisterActivity.class.getName();
    @BindView(R.id.edit_email_register_activity)
    EditText mEditEmail;
    @BindView(R.id.edit_user_register_activity)
    EditText mEditUsername;
    @BindView(R.id.edit_password_register_activity)
    EditText mEditPassword;
    @BindView(R.id.edit_password_confirm_register_activity)
    EditText mEditPasswordConfirm;
    @BindView(R.id.edit_fullname_register_activity)
    EditText mEditFullName;
    @BindView(R.id.progressBar_register)
    ProgressBar mProgressBar;

    RegisterComponent registerComponent;
    @Inject
    RegisterPresenter registerPresenter;

    private User user;

    public static Intent getCallIntent(Context context) {
        return new Intent(context, RegisterActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        this.user = new User();
        setUpView();
        initEditTextFocus();
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.registerComponent.inject(this);
        this.registerPresenter.setRegisterView(this);
    }


    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.registerComponent = DaggerRegisterComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void initEditTextFocus() {
        mEditEmail.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditEmail.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditEmail.setBackgroundResource(R.drawable.edit_style);
            }
        });
        mEditUsername.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditUsername.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditUsername.setBackgroundResource(R.drawable.edit_style);
            }
        });
        mEditPassword.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditPassword.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditPassword.setBackgroundResource(R.drawable.edit_style);
            }
        });
        mEditPasswordConfirm.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditPasswordConfirm.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditPasswordConfirm.setBackgroundResource(R.drawable.edit_style);
            }
        });
        mEditFullName.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditFullName.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditFullName.setBackgroundResource(R.drawable.edit_style);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.registerPresenter.destroy();
    }

    @OnClick(R.id.button_register_activity)
    public void clickButtonRegister() {
        if (validateEdiText()) {
            Log.d(TAG, mEditUsername.getText().toString());
            this.user.setUsername(mEditUsername.getText().toString().trim());
            this.user.setPassword(mEditPassword.getText().toString().trim());
            this.user.setName(mEditFullName.getText().toString().trim());
            this.user.setEmail(mEditEmail.getText().toString().trim());
            this.createAccount();
        } else {
            this.showToastMessage("Ingrese datos");
            //this.showSnackBarMessage(this, "Error", Snackbar.LENGTH_LONG);
        }
    }

    private boolean validateEdiText() {
        if (!mEditUsername.getText().toString().isEmpty() && !mEditPassword.getText().toString().isEmpty()) {
            return true;
        }
        return false;
    }

    public void createAccount() {
        this.registerPresenter.createUserAccount(this.user);
    }

    private void enableComponents(Boolean enable) {
        mEditFullName.setEnabled(enable);
        mEditEmail.setEnabled(enable);
        mEditPassword.setEnabled(enable);
        mEditPassword.setEnabled(enable);
        mEditPasswordConfirm.setEnabled(enable);
        mEditUsername.setEnabled(enable);
    }

    @Override
    public void goToLogin() {
        startActivity(LoginActivity.getCallIntent(this));
    }

    @Override
    public void showLoading() {
        enableComponents(false);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.INVISIBLE);
        enableComponents(true);
    }

    @Override
    public void showError(String messageError) {
        showToastMessage(messageError);
        mProgressBar.setVisibility(View.INVISIBLE);
        enableComponents(true);
    }
}
