package com.olivares.smartgrow.di.components;

import com.olivares.smartgrow.views.plagues.PlaguesFragment;
import com.olivares.smartgrow.views.custom.ImageFullScreenScreenActivity;
import com.olivares.smartgrow.di.modules.ActivityModule;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.plagues.detail.PlagueDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface LibraryComponent {
    void inject(PlaguesFragment plaguesFragment);

    void inject(PlagueDetailActivity plagueDetailActivity);

    void inject(ImageFullScreenScreenActivity imageFullScreenActivity);

}
