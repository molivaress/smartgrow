package com.olivares.smartgrow.views;

public interface SwipeRefreshView {
    void showRefresh();

    void hideRefresh();

    void showRefreshError(String messageError);
}
