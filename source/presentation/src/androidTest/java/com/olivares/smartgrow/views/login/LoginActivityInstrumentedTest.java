package com.olivares.smartgrow.views.login;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ProgressBar;

import com.olivares.smartgrow.R;
import com.olivares.smartgrow.views.Util;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertNotNull;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class LoginActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule =
            new ActivityTestRule<LoginActivity>(LoginActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    return new Intent();
                }
            };
    private LoginActivity loginActivity;

    @Before
    public void setUp() {
        this.loginActivity = loginActivityActivityTestRule.getActivity();
    }

    @Test
    public void testLaunchLogin() {
        View mEditUsername = this.loginActivity.findViewById(R.id.edit_user_login_activity);
        View mEditPassword = this.loginActivity.findViewById(R.id.edit_password_login_activity);
        View mButtonLogin = this.loginActivity.findViewById(R.id.button_login_activity);
        View mTextForgotPassword = this.loginActivity.findViewById(R.id.text_forgot_password_login_activity);
        View mProgressBar = this.loginActivity.findViewById(R.id.progressBar_login_activity);
        assertNotNull(mEditUsername);
        assertNotNull(mEditPassword);
        assertNotNull(mButtonLogin);
        assertNotNull(mTextForgotPassword);
        assertNotNull(mProgressBar);
    }

    @Test
    public void testLoginCorrect() throws Exception {
        onView(withId(R.id.edit_user_login_activity)).perform(typeText("molivars"), closeSoftKeyboard());
        onView(withId(R.id.edit_password_login_activity)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.button_login_activity)).perform(click());
        Util.await(5000);
        onView(withId(R.id.recycler_history_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void testLoginIncorrect() throws Exception {
        onView(withId(R.id.button_login_activity)).perform(click());
        onView(withText(R.string.text_enter_password)).inRoot(RootMatchers.withDecorView(
                CoreMatchers.not(CoreMatchers.is(this.loginActivity
                        .getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }


}
