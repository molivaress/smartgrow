package com.olivares.remote.network;

import com.olivares.remote.BuildConfig;
import com.olivares.remote.entity.response.ImageEntityResponse;
import com.olivares.remote.entity.request.ImageEntityRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RestApi2 {

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_UPLOAD_IMAGE)
    Call<ImageEntityResponse> uploadImage(@Header("Authorization") String value, @Body ImageEntityRequest imageEntityRequest);

}
