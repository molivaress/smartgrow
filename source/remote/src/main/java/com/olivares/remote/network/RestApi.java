package com.olivares.remote.network;

import com.olivares.remote.BuildConfig;
import com.olivares.remote.entity.request.BaseEntityRequest;
import com.olivares.remote.entity.request.CreateAccountEntityRequest;
import com.olivares.remote.entity.request.DetailEntityRequest;
import com.olivares.remote.entity.response.DetailEntityResponseResponse;
import com.olivares.remote.entity.response.HistoryEntityResponseResponse;
import com.olivares.remote.entity.response.PlaguesEntityResponseResponse;
import com.olivares.remote.entity.response.RecognizeEntityResponseResponse;
import com.olivares.remote.entity.request.RecognizeEntityRequest;
import com.olivares.remote.entity.response.UserEntityResponse;
import com.olivares.remote.entity.request.LoginUserEntityRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestApi {

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_LOGIN)
    Call<UserEntityResponse> loginUser(@Body LoginUserEntityRequest loginUserEntityRequest);

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_REGISTER)
    Call<UserEntityResponse> createAccountUser(@Body CreateAccountEntityRequest createAccountEntityRequest);

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_RECOGNITION_IMAGE)
    Call<RecognizeEntityResponseResponse> recognitionImage(@Body RecognizeEntityRequest recognizeEntityRequest);

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_DETAIL)
    Call<DetailEntityResponseResponse> getDetail(@Path("id") int user, @Body DetailEntityRequest detailEntityRequest);

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_PLAGUES)
    Call<PlaguesEntityResponseResponse> getLibrary(@Body BaseEntityRequest baseEntityRequest);

    @Headers("Accept: application/json")
    @POST(BuildConfig.API_URL_HISTORY_RECOGNITIONS)
    Call<HistoryEntityResponseResponse> getHistory(@Path("user") String user, @Body BaseEntityRequest baseEntityRequest);

}
