package com.olivares.remote.cloud.plagues;

import android.util.Log;

import com.olivares.entities.Plague;
import com.olivares.remote.entity.request.BaseEntityRequest;
import com.olivares.remote.entity.request.DetailEntityRequest;
import com.olivares.remote.entity.response.DetailEntityResponseResponse;
import com.olivares.remote.entity.response.PlaguesEntityResponseResponse;
import com.olivares.remote.network.RestApi;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryRemote;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;

public class CloudPlaguesRepository implements PlaguesRepositoryRemote {
    private static final String TAG = CloudPlaguesRepository.class.getName();
    private final RestApi restApi;

    @Inject
    public CloudPlaguesRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Plague getPlagueData(int plagueId) throws Exception {
        try {
            Call<DetailEntityResponseResponse> queryEntityCall = restApi.getDetail(plagueId, new DetailEntityRequest("molivars", plagueId));
            DetailEntityResponseResponse detailEntityRequest = queryEntityCall.execute().body();
            Log.d(TAG, detailEntityRequest.getStatusEntity().getMessage() + "");
            Plague plague = null;
            if (detailEntityRequest != null && detailEntityRequest.getStatusEntity().getCode() == 1) {
                Log.d(TAG, detailEntityRequest.getStatusEntity().getMessage());
                plague = detailEntityRequest.toPlague();
                return plague;
            } else {
                throw new Exception("error al realizar la clasificación.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Plague> getPlagues() throws Exception {
        try {
            Call<PlaguesEntityResponseResponse> queryEntityCall = restApi.getLibrary(new BaseEntityRequest("molivars"));
            PlaguesEntityResponseResponse libraryEntityResponse = queryEntityCall.execute().body();
            Log.d(TAG, libraryEntityResponse.getStatusEntity().getMessage() + " ");
            List<Plague> plagueList = null;
            if (libraryEntityResponse != null && libraryEntityResponse.getStatusEntity().getCode() == 1) {
                plagueList = libraryEntityResponse.toPlagueList();
            } else {
                throw new Exception("eError al realizar el reconocimiento.");
            }
            return plagueList;
        } catch (Exception e) {
            throw e;
        }
    }
}
