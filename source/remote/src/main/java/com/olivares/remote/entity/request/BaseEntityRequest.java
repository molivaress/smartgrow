package com.olivares.remote.entity.request;

public class BaseEntityRequest {
    private String user;

    public BaseEntityRequest(String user) {
        this.user = user;
    }

    public BaseEntityRequest() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
