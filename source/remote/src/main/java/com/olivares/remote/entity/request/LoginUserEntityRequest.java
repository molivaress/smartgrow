package com.olivares.remote.entity.request;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.User;

public class LoginUserEntityRequest {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;

    public LoginUserEntityRequest() {
    }

    public LoginUserEntityRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginUserEntityRequest(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
