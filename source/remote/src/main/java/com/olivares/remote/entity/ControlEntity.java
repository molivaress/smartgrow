package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

public class ControlEntity {
    @SerializedName("type_control")
    private int type;
    @SerializedName("description_control")
    private String description;

    public ControlEntity(int type, String description) {
        this.type = type;
        this.description = description;
    }

    public ControlEntity() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
