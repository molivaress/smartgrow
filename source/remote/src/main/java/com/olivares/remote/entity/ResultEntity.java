package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

public class ResultEntity {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String descriptionPlague;
    @SerializedName("name_clarifai")
    private String name_clarifai;
    @SerializedName("value")
    private double value;
    @SerializedName("url_image_plague")
    private String url_image_plague;
    @SerializedName("plague_id")
    private int plague_id;

    public ResultEntity(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public ResultEntity(String name, String name_clarifai, double value, String url_image_plague, int plague_id) {
        this.name = name;
        this.name_clarifai = name_clarifai;
        this.value = value;
        this.url_image_plague = url_image_plague;
        this.plague_id = plague_id;
    }

    public ResultEntity() {
    }

    public String getName_clarifai() {
        return name_clarifai;
    }

    public void setName_clarifai(String name_clarifai) {
        this.name_clarifai = name_clarifai;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionPlague() {

        return descriptionPlague;
    }

    public void setDescriptionPlague(String descriptionPlague) {
        this.descriptionPlague = descriptionPlague;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUrl_image() {
        return url_image_plague;
    }

    public void setUrl_image(String url_image_plague) {
        this.url_image_plague = url_image_plague;
    }

    public int getPlague_id() {
        return plague_id;
    }

    public void setPlague_id(int plague_id) {
        this.plague_id = plague_id;
    }
}
