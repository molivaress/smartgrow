package com.olivares.remote.entity.request;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.User;

public class CreateAccountEntityRequest extends LoginUserEntityRequest {
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;

    public CreateAccountEntityRequest(User user) {
        super(user.getUsername(), user.getPassword());
        this.name = user.getName();
        this.email = user.getEmail();
    }

    public CreateAccountEntityRequest(String username, String password, String name, String email) {
        super(username, password);
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
