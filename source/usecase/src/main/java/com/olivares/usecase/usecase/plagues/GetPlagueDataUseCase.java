package com.olivares.usecase.usecase.plagues;

import com.olivares.entities.Plague;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryRemote;

import javax.inject.Inject;

public class GetPlagueDataUseCase {
    private static final String TAG = GetPlaguesUseCase.class.getName();
    private final PlaguesRepositoryRemote plaguesRepositoryRemote;
    private final PlaguesRepositoryLocal plaguesRepositoryLocal;

    @Inject
    public GetPlagueDataUseCase(PlaguesRepositoryRemote plaguesRepositoryRemote,
                                PlaguesRepositoryLocal plaguesRepositoryLocal) {
        this.plaguesRepositoryRemote = plaguesRepositoryRemote;
        this.plaguesRepositoryLocal = plaguesRepositoryLocal;
    }

    public Plague getPlagueData(int plagueId) throws Exception {
        return this.plaguesRepositoryLocal.getPlague(plagueId);
    }
}
