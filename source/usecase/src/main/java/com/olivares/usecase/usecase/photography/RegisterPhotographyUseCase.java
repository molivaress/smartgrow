package com.olivares.usecase.usecase.photography;

import com.olivares.entities.Image;
import com.olivares.usecase.repository.photography.PhotographyRepositoryRemote;

import javax.inject.Inject;

public class RegisterPhotographyUseCase {
    private static final String TAG = RegisterPhotographyUseCase.class.getName();
    private final PhotographyRepositoryRemote photographyRepositoryRemote;

    @Inject
    public RegisterPhotographyUseCase(PhotographyRepositoryRemote photographyRepositoryRemote) {
        this.photographyRepositoryRemote = photographyRepositoryRemote;
    }

    public Image uploadPhotography(Image image) throws Exception {
        return this.photographyRepositoryRemote.uploadPhotography(image);
    }
}
