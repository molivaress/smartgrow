package com.olivares.entities;

public class AlertItem {
    private String txtGoodImage;
    private String txtBadImage;
    private int goodImage;
    private int badImage;

    public AlertItem(String txtGoodImage, String txtBadImage, int goodImage, int badImage) {
        this.txtGoodImage = txtGoodImage;
        this.txtBadImage = txtBadImage;
        this.goodImage = goodImage;
        this.badImage = badImage;
    }

    public AlertItem() {
    }

    public String getTxtGoodImage() {
        return txtGoodImage;
    }

    public void setTxtGoodImage(String txtGoodImage) {
        this.txtGoodImage = txtGoodImage;
    }

    public String getTxtBadImage() {
        return txtBadImage;
    }

    public void setTxtBadImage(String txtBadImage) {
        this.txtBadImage = txtBadImage;
    }

    public int getGoodImage() {
        return goodImage;
    }

    public void setGoodImage(int goodImage) {
        this.goodImage = goodImage;
    }

    public int getBadImage() {
        return badImage;
    }

    public void setBadImage(int badImage) {
        this.badImage = badImage;
    }
}
