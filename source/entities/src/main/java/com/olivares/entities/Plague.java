package com.olivares.entities;

import java.util.ArrayList;
import java.util.List;

public class Plague {
    private int plagueId;
    private String name;
    private String description;
    private List<Control> controlList;
    private String urlImage;
    private String nameClarifai;

    public Plague() {
        controlList = new ArrayList<>();
    }

    public Plague(String name, String description) {
        this.name = name;
        this.description = description;
        controlList = new ArrayList<>();
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Control> getControlList() {
        return controlList;
    }

    public void addControl(Control control) {
        this.controlList.add(control);
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }

    @Override
    public String toString() {
        return name + " " + description;
    }
}
