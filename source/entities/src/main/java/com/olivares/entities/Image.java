package com.olivares.entities;

public class Image {
    private String link;
    private String name;
    private String image;

    public Image(String link) {
        this.link = link;
    }

    public Image(String name, String image) {
        this.name = name;
        this.image = image;
    }


    public Image() {

    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
