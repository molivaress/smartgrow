package com.olivares.entities;

public class Control {
    private int type;
    private String description;

    public Control(String description, int type) {
        this.description = description;
        this.type = type;
    }

    public Control() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
