package com.olivares.local.db;

import com.olivares.entities.Plague;
import com.olivares.local.entities.PlagueEntity;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

public class DbPlagueRepository implements PlaguesRepositoryLocal {
    private static final String TAG = DbPlagueRepository.class.getName();
    private final RealmConfiguration realmConfiguration;

    @Inject
    public DbPlagueRepository(RealmConfiguration realmConfiguration) {
        this.realmConfiguration = realmConfiguration;
    }

    @Override
    public Boolean savePlaguesLocal(List<Plague> plagues) {
        try (Realm realm = Realm.getInstance(realmConfiguration)) {
            final RealmList<PlagueEntity> plagueEntities = PlagueEntity.toPlagueEntities(plagues);
            realm.executeTransaction(realm1 -> realm1.insert(plagueEntities));
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Plague> getPlaguesLocal() {
        try (Realm realm = Realm.getInstance(realmConfiguration)) {
            RealmResults<PlagueEntity> plagueEntities = realm.where(PlagueEntity.class).findAll();
            return PlagueEntity.toPlagues(plagueEntities);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Plague getPlague(int plagueId) {
        try (Realm realm = Realm.getInstance(realmConfiguration)) {
            PlagueEntity plagueEntity = realm.where(PlagueEntity.class).equalTo("plagueId", plagueId).findFirst();
            return PlagueEntity.toPlague(plagueEntity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Boolean deleteAllPlaguesLocal() throws Exception {
        try (Realm realmInstance = Realm.getInstance(realmConfiguration)) {
            realmInstance.executeTransaction(realm -> realm.delete(PlagueEntity.class));
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
