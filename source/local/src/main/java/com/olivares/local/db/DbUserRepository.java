package com.olivares.local.db;

import android.util.Log;

import com.olivares.entities.User;
import com.olivares.local.entities.UserEntity;
import com.olivares.usecase.repository.user.UserRepositoryLocal;

import java.util.Collections;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DbUserRepository implements UserRepositoryLocal {
    private final String TAG = DbUserRepository.class.getName();
    private final RealmConfiguration realmConfiguration;

    @Inject
    public DbUserRepository(RealmConfiguration realmConfiguration) {
        this.realmConfiguration = realmConfiguration;
    }

    @Override
    public User getUser() throws Exception {
        Realm realm = null;
        try {
            realm = Realm.getInstance(realmConfiguration);
            UserEntity userEntity = realm.where(UserEntity.class).findFirst();
            if (userEntity != null) {
                return userEntity.toUser();
            } else {
                throw new Exception("NOT_FOUND");
            }
        } catch (Exception e) {
            Log.d(TAG, "Excepcion: " + e.getMessage());
            throw e;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    @Override
    public Boolean saveUser(User user) throws Exception {
        try (Realm realmInstance = Realm.getInstance(realmConfiguration)) {
            realmInstance.executeTransaction(realm -> realm.insertOrUpdate(new UserEntity(user)));
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Boolean deleteAllUser() throws Exception {
        try (Realm realmInstance = Realm.getInstance(realmConfiguration)) {
            realmInstance.executeTransaction(realm -> realm.delete(UserEntity.class));
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
